# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 17:03:02 2018

@author: elefther
"""
import booleans as bl

n = 10  # dimension of the cube = number of variables

## Define a random DNF that has 10 terms, each of them with 10 stars

## find a sample of size 100
from fixed_input_big import S
#S = bl.sample_from_DNF_u(10,*DNF)   # better method for large n
print 'traning sample |S| =',len(S),'defined'
print S
T=bl.Table(S)
T.learn_kDNF(50, print_on = False, print_latex = False)

#for i in xrange(len(S)-1):
#    j = len(T.partitions)
#    print 'joins=%d  cost=%d    size = (%d,%d,%d)  error=%.4f' %(row['number of merges'],row['total_cost'],row['size'],row['min_order'],row['max_order'],row['error1'])
#    print T
#    T.step('f')
#  
#print T

res= '\n\n'.join(T.terms)

#res= '================================'+ '\n'.join(T.terms)+'\n================================'
f = open('result50', 'w')
f.write(res)  # python will convert \n to os.linesep
f.close()
