# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:23:36 2017
last update on Thu 15 Feb 18

@author: elefther

Define Boolean Cubes (terms) and define properties and operations

"""
import numpy as np
import random
import time

import matplotlib
# import numpy as np
import matplotlib.pyplot as plt


# ==============================================================================
# Define the Cube
# ==============================================================================
def cube(n):
    """The alphabet. Returns the set of all points in B^n """
    assert type(n) == int and n >= 0
    s = '{0:0>' + str(n) + '}'
    return tuple([s.format(bin(i)[2:]) for i in xrange(2 ** n)])


# ==============================================================================
# Terms
# ==============================================================================

class term:
    """A term is a sequence of certain length that caontains symbols from {0,1,*}.
    The symbol * is used as a DON'T CARE symbol, that is, whenever a variable is of no interest and can take either value 0 or 1. """

    def __init__(self, vector):
        assert all([i in '01*' for i in vector])
        self.vector = vector

    def dim(self):
        """Dimension of Boolean Cube"""
        return len(self.vector)

    def tau(self):
        """returns the type of the term, i.e., the set of DON'T CARE variables"""
        return [i for i in range(self.dim()) if self.vector[i] == '*']

    def order(self):
        """The number of DON'T CARE variables"""
        return self.vector.count('*')

    def size(self):
        """Cardinality of semantics"""
        return 2 ** self.order()

    def is_minterm(self):
        """Returns True if the term is a minterm, False otherwise"""
        return not self.order()

    def semantics(self):
        return semantics(self.vector)

    def info(self):
        print 'Term %r, of dimension %d, is %sa minterm.' % (
            self.vector, self.dim(), str(np.where(self.is_minterm(), '', 'not '))),
        print 'It is of type %s and has order %d.' % (self.tau(), self.order())

    def __str__(self):
        return self.vector


# def semantics(vector):
#    stars = vector.count('*')
#    if stars == 0:
#        return [vector]
#    else:
#        return semantics(vector.replace('*','0',1)) + semantics(vector.replace('*','1',1))


def semantics(*terms):
    sem = set()
    for vector in terms:
        stars = vector.count('*')
        if stars == 0:
            sem.add(vector)
        else:
            sem |= semantics(vector.replace('*', '0', 1), vector.replace('*', '1', 1))
    return sem


def sem_size_appr(*terms):
    return sum([2 ** term.count('*') for term in terms])


def sem_size_exact(*terms):
    return len(semantics(*terms))


# ==============================================================================
# Operations on Terms
# ==============================================================================

bit_cap = {('0', '0'): '0', ('0', '*'): '0',
           ('1', '1'): '1', ('1', '*'): '1',
           ('*', '0'): '0', ('*', '1'): '1', ('*', '*'): '*'}

bit_join = {('0', '0'): '0', ('0', '1'): '*', ('0', '*'): '*',
            ('1', '0'): '*', ('1', '1'): '1', ('1', '*'): '*',
            ('*', '0'): '*', ('*', '1'): '*', ('*', '*'): '*'}

bit_incl = {('0', '0'): True, ('0', '1'): False, ('0', '*'): True,
            ('1', '0'): False, ('1', '1'): True, ('1', '*'): True,
            ('*', '0'): False, ('*', '1'): False, ('*', '*'): True}

bit_hsdf = {('0', '0'): 0, ('0', '1'): 1, ('0', '*'): 0,
            ('1', '0'): 1, ('1', '1'): 0, ('1', '*'): 0,
            ('*', '0'): 1, ('*', '1'): 1, ('*', '*'): 0}

bit_hamm = {('0', '0'): 0, ('0', '1'): 1, ('0', '*'): 0,
            ('1', '0'): 1, ('1', '1'): 0, ('1', '*'): 0,
            ('*', '0'): 0, ('*', '1'): 0, ('*', '*'): 0}


def check(term1, term2):
    return term1.dim() == term2.dim()


def hamming(term1, term2):
    """What is the Hamming distance of two terms"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum(i != j and not ('*' in (i, j)) for i, j in zip(term1.vector, term2.vector))


def hamming2(term1, term2):
    """The Hamming distance of two terms computed bitwise from operation matrix"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum([bit_hamm.get((i, j)) for i, j in zip(term1.vector, term2.vector)])


def hausdorf_v(term1, term2):
    """The Hausdorf distance of two terms"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum([bit_hsdf.get((i, j)) for i, j in zip(term1.vector, term2.vector)])


def hausdorf(term1, term2):
    return max(hausdorf_v(term1, term2), hausdorf_v(term2, term1))


def intersection(term1, term2):
    """return a term that represents the intersection of two terms"""
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    r = [bit_cap.get((i, j), None) for i, j in zip(term1.vector, term2.vector)]
    if None in r:
        return None
    else:
        return term(''.join(r))


# def union(term1,term2):
#    # check: the two terms belong to the same domain
#    assert term1.dim() == term2.dim()
#    pass

def inclusion(term1, term2):
    """return True (resp. False) when term1 is (resp. is not) included in term2"""
    assert check(term1, term2)
    return all([bit_incl[i] for i in zip(term1.vector, term2.vector)])


def join(term1, term2):
    """return a new term that represents the join of the two given terms"""
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    r = [bit_join.get((i, j), None) for i, j in zip(term1.vector, term2.vector)]
    return term(''.join(r))


def join_cost(term1, term2):
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    inters = intersection(term1, term2)
    if inters is None:
        return join(term1, term2).size() - term1.size() - term2.size()
    else:
        return join(term1, term2).size() - term1.size() - term2.size() + intersection(term1, term2).size()


def size(term1, term2):
    """the size of the join defined as an external function"""
    assert check(term1, term2)
    return join(term1, term2).size()


##==============================================================================
## Define the graph of a Cube
##==============================================================================
#
import networkx as nx


def hypercube(n):
    """Define a hypercube of dimension n"""
    Q = nx.Graph()
    Q.add_edge(0, 1)

    dim = 1
    while dim < n:
        # add one dimension to the hypercube
        Q = nx.cartesian_product(Q, nx.Graph([(0, 1)]))
        dim += 1
    # renaming the nodes
    d = {a: ''.join([i for i in repr(a) if i not in "()', "]) for a in Q.nodes()}

    return nx.relabel_nodes(Q, d)


#
#
##==============================================================================
## Graphical representation of two terms
##==============================================================================
#
def make_graph(term1, term2):
    """Make a graph for a term or union of terms"""
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    A = hypercube(term1.dim())
    val_map = {}

    # paint nodes of term blue
    val_map.update({n: 0.3 for n in term1.semantics()})

    val_map.update({n: 0.5 for n in term2.semantics()})
    if not intersection(term1, term2) is None:
        val_map.update({n: 0.4 for n in intersection(term1, term2).semantics()})
    node_colors = [val_map.get(node, 0.0) for node in A.nodes()]
    nx.draw(A, node_size=100, cmap=plt.get_cmap('jet'), node_color=node_colors)


# ==============================================================================
# Vector inclusion in DNF (set of terms)
# ==============================================================================

def inclusion_dnf(vector, *dnf):
    """A Boolean function that return True when the vector is part of the DNF, False otherwise"""
    vector = term(vector)
    for a in dnf:
        if inclusion(vector, term(a)):
            return True
    return False


# ==============================================================================
# Counting the error
# ==============================================================================


def approx_difference(dnf1, dnf2, num_tests=100):
    """compute the distance between two DNFs.
    DNF1 stands for the target and DNF2 for the hypothesis.
    """
    n = len(dnf1[0])  # the dimension
    assert len(dnf2[0]) == n

    fals_neg = 0
    fals_pos = 0

    for i in xrange(num_tests):
        test_vector = random_vector(n)

        test1 = inclusion_dnf(test_vector, *dnf1)
        test2 = inclusion_dnf(test_vector, *dnf2)

        if test1 and not test2:
            fals_neg += 1
        elif not test1 and test2:
            fals_pos += 1

    return (fals_neg + fals_pos) / float(num_tests)


def approx_difference2(dnf1, dnf2):
    """compute the distance between two DNFs.
    DNF1 stands for the target and DNF2 for the hypothesis.
    """
    n = len(dnf1[0])  # the dimension
    assert len(dnf2[0]) == n

    num_tests = 1000

    fals_neg = 0
    fals_pos = 0

    for i in xrange(num_tests):
        test_vector = random_vector(n)

        test1 = inclusion_dnf(test_vector, *dnf1)
        test2 = inclusion_dnf(test_vector, *dnf2)

        if test1 and not test2:
            fals_neg += 1
        elif not test1 and test2:
            fals_pos += 1

    return (fals_neg + fals_pos) / float(num_tests)


def false_neg(target_dnf, hypothesis_dnf, num_tests=100):
    """on a sample taken from the hypothesis, returns the number of vectors that do not belong to the target, i.e., false positives.
    """
    fals_neg = 0

    for test_vector in sample_from_DNF_u(num_tests, *target_dnf):
        if not inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_neg += 1

    return float(fals_neg) / num_tests


def false_pos(target_dnf, hypothesis_dnf, num_tests=100):
    """on a sample taken from the target, returns the number of vectors that do not belong to the hypothesis, i.e., false negatives.
    """
    fals_pos = 0

    for i in xrange(num_tests):
        test_vector = sample_from_DNF_u(1, *hypothesis_dnf)[0]
        if not inclusion_dnf(test_vector, *target_dnf):
            fals_pos += 1

    return float(fals_pos) / num_tests


def false_neg2(testing_sample, hypothesis_dnf):
    """on a given testing sample, check what is the percentage of vectors NOT belonging to the hypothesis.
    """
    fals_neg = 0

    for test_vector in testing_sample:
        if not inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_neg += 1

    return float(fals_neg) / len(testing_sample)


def false_pos2(testing_sample, hypothesis_dnf):
    """on a given testing sample, check what is the percentage of vectors belonging to the hypothesis.
    """
    fals_pos = 0

    for test_vector in testing_sample:
        if inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_pos += 1

    return float(fals_pos) / len(testing_sample)


# check by counting : find and compare the semantics
def exact_difference(dnf1, dnf2):
    """returns the normalized volume of the symmetric difference"""
    n = len(dnf1[0])
    assert len(dnf2[0]) == n
    sem1 = semantics(*dnf1)
    sem2 = semantics(*dnf2)
    diff = sem1.symmetric_difference(sem2)
    return len(diff) / float(2 ** n)


def error(hypothesis, testing_sample):
    """the error is given as an approximation of
    the false negatives of the hypothesis,
    given a testing sample of positive examples

        p(h'|f)

    """
    a = len(testing_sample)  # |S1|
    c = len([i for i in testing_sample if inclusion_dnf(i, *hypothesis)])  # |S1 \cap h|

    # p(h'|f) = The probability of a vector to be outside h, given that it is in f
    return float(a - c) / a


def error2(hypothesis, testing_sample_neg):
    """the error is given as an approximation of
    the false positives of the hypothesis,
    given a testing sample of negative examples

        p(h|f')

    """
    a = len(testing_sample_neg)  # |S1|
    c = len([i for i in testing_sample_neg if inclusion_dnf(i, *hypothesis)])  # |S1 \cap h|

    # p(h'|f) = The probability of a vector to be outside h, given that it is in f
    return float(c) / a


# ==============================================================================
# Boolean Function Inference
# ==============================================================================


class Table:
    """The data structure where we keep all information and do the calculations"""

    foo = {'dist': hausdorf, 'size': size, 'cost': join_cost, 'join': join}

    def __init__(self, sample, cost_function='cost'):
        """ Sample and cost functions are provided.
        The cost function can either be the 'join_cost' or the 'hamming distance'.
        A table calculates the cost for all pairs of data
        """
        assert cost_function in ['dist', 'cost'], " Cost function can either be the 'join_cost' or the hamming distance"

        self.terms = list(sample)

        self.cost_function = self.foo[cost_function]

        # a dictionary that will trace costs of all indices
        self.cost = {}
        self.update_cost()

        self.total_cost = 0
        self.timer = 0

        self.partitions = {i: {i} for i in sample}

    def reinitialization(self):
        self.__init__(self.initial_sample())

    def update_cost(self):
        for i, j in [(i, j) for i in self.terms for j in self.terms[self.terms.index(i) + 1:]]:
            self.cost[(i, j)] = self.cost_function(term(i), term(j))

    def current_sample(self):
        return self.partitions.keys()

    def initial_sample(self):
        return [j for i in self.partitions.values() for j in i]

    # old name learn_kDNF
    def klearn(self, k, select='f', print_on=True, print_latex=False):
        """Returns a k-term DNF, i.e., a set of k terms"""
        while len(self.partitions) > k:
            print len(self.partitions)
            if print_on: print self.__str__()
            if print_latex: print self.latex_output()
            self.step(select)

        if print_on: print self.__str__()
        if print_latex: print self.latex_output()

    # old name learn_e
    def elearn(self, testing_sample, e=0.1, select='f', print_on=True, print_latex=False):
        """Returns a k-term DNF, where k is unknown. The merging stops when
        the error (false negatives) become smaller than a given epsilon e (default value 0.1).
        The false negatives are computed based on some testing sample S1"""
        false_neg = error(self.terms, testing_sample)
        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [false_neg]
        while false_neg > e and len(self.partitions) > 1:
            print "partitions: %d, error: %.4f" % (len(self.partitions), false_neg)
            if print_on: print self.__str__()
            if print_latex: print self.latex_output()
            self.step(select)
            false_neg = error(self.terms, testing_sample)
            X.append(S - len(self.partitions))
            Y.append(false_neg)
        print "partitions: %d, error: %.4f" % (len(self.partitions), false_neg)
        if print_on: print self.__str__()
        if print_latex: print self.latex_output()
        return X, Y

    def dlearn(self, threshold=2, select='f', print_on=True, print_latex=False):
        """Returns a k-term DNF, where k is unknown. The merging stops when
        the size of the hypothesis function makes a jump that is higher than a threshold. """
        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [sem_size_appr(*self.terms)]
        while len(self.partitions) > 1:
            print "partitions: %d, size: %d" % (len(self.partitions), Y[-1])
            if print_on: print self.__str__()
            if print_latex: print self.latex_output()
            self.step(select)
            X.append(S - len(self.partitions))
            Y.append(sem_size_appr(*self.terms))

            print Y[-1] / float(Y[-2])
            if Y[-1] / float(Y[-2]) > 2:
                break

        print "partitions: %d, size: %d" % (len(self.partitions), Y[-1])
        if print_on: print self.__str__()
        if print_latex: print self.latex_output()
        return X, Y

    def step(self, select='f'):
        """finds a pair with minimal cost, and merge the terms"""
        t0_step = time.clock()
        if select in ['r', 'random']:
            ## select randomly among all the possible pairs
            tuple2merge = random.choice(self.mins())
        else:  # 'f' or 'first' or other then take first (which is faster)
            ## find the first pair of terms for which cost is minimum
            tuple2merge = min(self.cost, key=self.cost.get)
        #            tuple2merge = self.mins()[0]

        a = term(tuple2merge[0])
        b = term(tuple2merge[1])

        print 'Partitions = %d. ' % (len(self.partitions)),
        print "Merge %s with %s\n" % (a.vector, b.vector)
        self.merge(a, b)

        self.timer += time.clock() - t0_step
        return a, b

    def merge(self, term1, term2):
        ### find join
        a = join(term1, term2)
        #        print a.vector
        self.total_cost += self.cost_function(term1, term2)

        ### update the sample
        # remove terms
        self.terms.remove(term1.vector)
        self.terms.remove(term2.vector)

        ### update the partition
        new_partition = self.partitions[term1.vector] | self.partitions[term2.vector]
        del self.partitions[term1.vector]
        del self.partitions[term2.vector]
        self.partitions[
            a.vector] = new_partition  # we need to put this after the deletion in case some partitions include some others

        ### update the cost table
        # delete old elements
        for key in [k for k in self.cost.keys() if term1.vector in k or term2.vector in k]:
            del self.cost[key]
        # add new ones
        for j in self.terms:
            b = term(j)
            self.cost[(b.vector, a.vector)] = self.cost_function(a, b)

        # add join to the sample
        self.terms.append(a.vector)

    def mins(self):
        """Returns all the pairs with min cost"""
        return [k for k, v in self.cost.items() if v == min(self.cost.values())]

    def full_table(self):
        """Creates a table (dictionary) that contains all information over joins, costs and sizes. """
        T = {'join': {}, 'size': {}}
        T['cost'] = self.cost
        for i, j in self.cost.keys():
            a = term(i)
            b = term(j)
            for key in ['join', 'size']:
                T[key][(a.vector, b.vector)] = self.foo[key](a, b)
        return T

    def latex_output(self):

        table = self.full_table()
        keys = table['cost'].keys()

        ## set length of columns
        columns_length = len(self.terms[0]) + 2
        table_rows = ['$$ \\begin{array}{| l |' + ' | '.join(['l' for i in self.terms]) + ' |} \n\\hline']

        table_rows.append(' ' * columns_length + ' & ' + ' & '.join(self.terms) + ' \\\\')
        table_rows.append('\\hline')

        for i in self.terms:
            row1 = []
            row2 = []
            for j in self.terms:
                if (i, j) in keys:
                    row1.append(str(table['join'][(i, j)]).center(columns_length))
                    row2.append(('(' + str(table['size'][(i, j)]) + ',' + str(table['cost'][(i, j)]) + ')').center(
                        columns_length))
                else:
                    row1.append('--'.center(columns_length))
                    row2.append(' '.center(columns_length))
            table_rows.append('  ' + i + ' &' + '&'.join(row1) + '\\\\')
            table_rows.append(' ' * columns_length + ' &' + '&'.join(row2) + '\\\\')
            table_rows.append('\\hline')

        table_rows.append('\\end{array} $$')
        s = ''
        for x in table_rows:
            s += x + '\n'
        return s

    def __str__(self):

        table = self.full_table()
        keys = table['cost'].keys()

        ## set length of columns
        columns_length = len(self.terms[0]) + 2

        table_rows = [' ' * columns_length + ' | ' + ' | '.join(self.terms)]
        table_rows.append('-' * len(table_rows[0]) + '-')

        ## fill in subsequent rows
        for i in self.terms:
            row1 = []
            row2 = []
            for j in self.terms:
                if (i, j) in keys:
                    row1.append(str(table['join'][(i, j)]).center(columns_length))
                    row2.append(('(' + str(table['size'][(i, j)]) + ',' + str(table['cost'][(i, j)]) + ')').center(
                        columns_length))
                else:
                    row1.append('--'.center(columns_length))
                    row2.append(' '.center(columns_length))
            table_rows.append('  ' + i + ' |' + '|'.join(row1))
            table_rows.append(' ' * columns_length + ' |' + '|'.join(row2))
            table_rows.append('-' * len(table_rows[0]) + '-')

        s = ''
        for x in table_rows:
            s += x + '\n'
        return s


def project(rectangle, *sample):
    """project a sample on the rectangle"""
    indices = [i for i in range(len(rectangle)) if rectangle[i] == '*']
    return [''.join([s[i] for i in indices]) for s in sample]


# ==============================================================================
# ==============================================================================
# # random functions
# ==============================================================================
# ==============================================================================


# ==============================================================================
# random objects
# ==============================================================================

# def random_vectorB(dim = 5):
#    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
#    """
#    return np.random.choice(cube(dim))

# def random_term(k,n):
#    """n is the number of variables in total, and k is the number of care variables in the term."""
#    assert k < n # the size of the sub-cube (term) is smaller than the dimension B^n
#    stars = random.sample(range(n),k)
#    term = ''
#    for i in range(n):
#        if i in stars: term += '*'
#        else: term += random.choice(['0','1'])
#    return term

# def random_term(k,n):
#    """n is the number of variables in total, and k is the number of care variables in the term."""
#    assert k < n # the size of the sub-cube (term) is smaller than the dimension B^n
#    a = [random.choice(['0','1']) for i in range(n-k)]+['*']*k
#    np.random.shuffle(a)
#    return ''.join(a)

def random_term(k, n):
    """n is the number of variables in total, and k is the number of care variables in the term."""
    assert k <= n, 'the size of the sub-cube (term) is greater than the dimension B^n'
    k0 = random.randint(0, n - k)
    a = ['0'] * k0 + ['1'] * (n - k - k0) + ['*'] * k
    np.random.shuffle(a)
    return ''.join(a)


def random_DNF(k, dim, min_order=0, max_order=None):
    """returns a set of k terms of dimension dim."""
    assert min_order >= 0
    if max_order is None: max_order = dim
    assert min_order <= max_order
    assert max_order <= dim

    DNF = set()
    while len(DNF) < k:
        DNF.add(random_term(random.randint(min_order, max_order), dim))
    return sorted(DNF)


# ==============================================================================
# random samples using semantics
# ==============================================================================

def random_sample(dim=5, size=8):
    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
    """
    return sorted(np.random.choice(cube(dim), size=size))


def sample_from_rectangle(rectangle, size):
    """returns a sample of given size that is chosen uniformly from the rectangle (term)"""
    assert size <= 2 ** rectangle.count('*')
    return sorted(random.sample(semantics(rectangle), size))


# def sample_from_DNF(dim = 10, sample_size = 8, k = 4):
#    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
#    """
#    assert k <= dim
#    DNF = random_DNF(k, dim, min_order = 2, max_order = 4)
#    sem = set([j for i in DNF for j in semantics(i)])
#    return random.sample(sem,sample_size)

def sample_from_DNF(sample_size=5, *terms):
    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
    """
    if terms == ():
        print 'sample chosen from a random DNF'
        terms = random_DNF(4, 10, min_order=2, max_order=4)
    sem = set([j for i in terms for j in semantics(i)])
    return sorted(random.sample(sem, sample_size))


# ==============================================================================
# random samples WITHOUT using the semantics
# ==============================================================================

def random_vector(n):
    """returns a random vector of size (#variables) n
    NOTE: faster from dimensions up to ~50, for longer vectors use random_vector2"""
    num_ones = random.randrange(n)
    vector = '1' * num_ones + '0' * (n - num_ones)
    return ''.join(random.sample(vector, len(vector)))


def random_vector2(n):
    """returns a random vector of size (#variables) n
    NOTE: faster from dimensions ~larger than 50, for shorter vectors use random_vector"""
    return ''.join(np.random.choice(['0', '1'], n))


# def random_vector2(n):
#    """returns a random vector of size (#variables) n"""
#    return ''.join([random.choice(['0','1']) for i in xrange(n)])

def random_sample_u(dim=5, size=8):
    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
    """
    assert size <= 2 ** dim
    sample = set()
    while len(sample) < size:
        sample.add(''.join([random.choice(['0', '1']) for i in range(dim)]))
    return sorted(sample)


def sample_from_rectangle_u(rectangle, size):
    """returns a sample of given size that is chosen uniformly from the rectangle (term)"""
    assert size <= 2 ** rectangle.count('*')
    sample = set()
    while len(sample) < size:
        sample.add(''.join([i if i in ['0', '1'] else random.choice(['0', '1']) for i in rectangle]))
    return sorted(sample)


# def sample_from_DNF(dim = 10, sample_size = 8, k = 4):
#    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
#    """
#    assert k <= dim
#    DNF = random_DNF(k, dim, min_order = 2, max_order = 4)
#    sem = set([j for i in DNF for j in semantics(i)])
#    return random.sample(sem,sample_size)

def sample_from_DNF_u(sample_size=5, *terms):
    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
    """
    assert sample_size <= sem_size_appr(*terms)
    sample = set()
    sizes = [2 ** (i.count('*')) for i in terms]
    prob = [float(i) / sum(sizes) for i in sizes]
    while len(sample) < sample_size:
        # select one rectangle
        rectangle = np.random.choice(terms, 1, p=prob)[0]  # change this to weighted probability instead of uniform
        # select uniformly one vector in it
        a = ''.join([i if i in ['0', '1'] else random.choice(['0', '1']) for i in rectangle])
        # rejection probability
        k = 1. / [inclusion(term(a), term(i)) for i in terms].count(True)
        # with prob 1/k update sample with new vector
        # otherwise continue with next iteration
        if bool(np.random.choice([True, False], 1, p=[k, 1 - k])):
            sample.add(a)

    return sorted(sample)


def sample_from_DNFc_u(sample_size=5, *terms):
    """From a random DNF, return a sample of size (default = 8) that belongs to its complement
    """
    n = len(terms[0])
    sample = set()
    while len(sample) < sample_size:
        vector = ''.join(np.random.choice(['0', '1'], n))
        if inclusion_dnf(vector, *terms):
            continue
        sample.add(vector)
    return sorted(sample)


# ==============================================================================
# fixed samples
# ==============================================================================

def fixed_sample():
    return ['10100', '10101', '10110', '10111', '11100', '11101', '11110', '11111']


def fixed_sample2():
    return ['111000', '011110', '001010', '010110', '101011', '101100', '001111', '001000', '000000']


def fixed_sample3():
    return ['111000', '001010', '101011', '101100', '001111', '01*110', '00*000']


def fixed_sample4():
    return ['111000', '011110', '001010']


# ==============================================================================
# main
# ==============================================================================

if __name__ == "__main__":
    # execute only if run as a script

    print 'A cube with 4 variables is the set ', cube(4)

    # define two terms
    a = term('*1*011')
    b = term('11**1*')
    print '\nWe define a term %r with the following properties:' % (a.vector)
    a.info()
    print '\nFor two terms %r and %r, we can compute the following:' % (a.vector, b.vector)
    print 'Intersection:', intersection(a, b)
    print 'Inclusion:', inclusion(a, b)
    print 'Join:', join(a, b)
    print 'join_cost:', join_cost(a, b)
    print 'hamming distance:', hamming(a, b)
    print 'hausdorf distance:', hausdorf_v(a, b)
    print 'symmetric hausdorf distance:', hausdorf(a, b)
    print

    print "The merging algorithm (using the join cost function)"
    print "A sample of size 5 is used to learn a rectangle (1-term DNF)"
    T = Table(sample_from_rectangle('11*0**', 5), cost_function='dist')
    # T.learn_kDNF(1)
    T.klearn(1)
