from BoolLearnLib.Table import *
from BoolLearnLib.Samples import *

from random import shuffle

# Defining the Sample
# Sample contains 8 terms that are summarized by the expression '11*0**', where * is a wild card representing
# either 0 or 1.
s = sample_from_rectangle('11*0**', 8)

shuffle(s)

# 1/3 of the sampling goes to training
training_sample = s[:(len(s) / 3)]
# 2/3 of the sampling goes to testing
testing_sample = s[1 + (len(s) / 3):]

# Initialize the learning function
T = Table(training_sample, cost_function='dist')

# The learning function stops when 'h' reaches k = 1 terms
T.klearn(1)
# Result
print('Result: {0}'.format(T.terms))
