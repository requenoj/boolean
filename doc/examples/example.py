from BoolLearnLib.Table import *
from BoolLearnLib.Samples import *

print('A cube with 4 variables is the set ', cube(4))

# define two terms
a = Term('*1*011')
b = Term('11**1*')
print('\nWe define a term {0} with the following properties:'.format(a.vector))
a.info()
print('\nFor two terms {0} and {1}, we can compute the following:'.format(a.vector, b.vector))
print('Intersection: ' + str(intersection(a, b)))
print('Inclusion: ' + str(inclusion(a, b)))
print('Join: ' + str(join(a, b)))
print('join_cost: ' + str(join_cost(a, b)))
print('hamming distance: ' + str(hamming(a, b)))
print('hausdorf distance: ' + str(hausdorf_v(a, b)))
print('symmetric hausdorf distance: ' + str(hausdorf(a, b)))
print

print('The merging algorithm (using the join cost function)')
print('A sample of size 5 is used to learn a rectangle (1-term DNF)')
T = Table(sample_from_rectangle('11*0**', 5), cost_function='dist')
# T.learn_kDNF(1)
T.klearn(1)

print('Result: {0}'.format(T.terms))
