from BoolLearnLib.Table import Table
from BoolLearnLib.Samples import sample_from_DNF_u, random_DNF
from BoolLearnLib.Error import plot_list_2D

from random import shuffle
from math import ceil, floor

import time

# Define the Sample
# Sample contains 5 terms that are summarized by the expression '11*0**', where * is a wild card representing
# either 0 or 1.
# s = sample_from_rectangle('**11*0**', 32)

lines_num_joins = []
lines_num_terms = []

# mink, maxk, stepk = (3, 4, 1)
mink, maxk, stepk = (10, 11, 1)
mind, maxd, stepd = (50, 51, 1)
mins, maxs, steps = (300, 310, 10)
# mins, maxs, steps = (300, 800, 100)
for i in range(1):
    for kterm in range(mink, maxk, stepk):
        for dim in range(mind, maxd, stepd):
            DNF = random_DNF(k=kterm, dim=dim, min_order=10, max_order=10)
            # DNF = DNF_simple_overlap(n=dim, d=dim-10)
            for si in range(mins, maxs, steps):
                # sem_size_DNF = sem_size_exact(*DNF)
                # sem_size_DNF = max(50, min(50, sem_size_appr(*DNF)/1000))
                sem_size_DNF = si
                # Sample S has the 10% of the size of the whole semantics of DNF
                S = sample_from_DNF_u(sem_size_DNF, *DNF)

                shuffle(S)

                # 1/3 of the sampling goes to training
                # training_sample = S[: int(ceil(len(S) / 3))]
                training_sample = S[: int(ceil(si/3))]

                # training_sample = training_sample.sort()

                # 2/3 of the sampling goes to testing
                # testing_sample = S[1 + int(floor(len(S) / 3)):]
                testing_sample = S[1 + int(floor(si/3)):]

                start = time.time()

                # Initialize the learning function
                T = Table(training_sample, cost_function='dist')

                # The learning function stops when 'h' reaches k = 1 terms
                # T.klearn(1, print_on=False, print_latex=False)
                # T.klearn(kterm, print_on=False, print_latex=False)

                # x, y = T.dlearn(threshold=2, select='f', print_on=False, print_latex=False)
                # x, y = T.dlearn(threshold=float('inf'), select='f', print_on=False, print_latex=False)

                x, y, z = T.elearn(testing_sample, print_on=False, print_latex=False)

                end = time.time()
                time0 = end - start


                # Result
                result = sorted(T.terms)
                expected = sorted(DNF)
                diff = (set(result) - set(expected)) | (set(expected) - set(result))

                print('Result: {0}'.format(str(result)))
                print('Expected: {0}'.format(str(expected)))
                print('Difference: {0}'.format(str(diff)))
                print('Execution time: wall clock {0}, user {1}'.format(str(time0), str(T.timer)))
                # time.sleep(5)

                # Number of joins
                # lines_num_joins = [(x1,y1),(x2,y2)]
                # lines_num_joins += [(x, y)] # dlearn
                lines_num_joins += [(x, z)] # elearn

                # Number of terms
                num_terms = [(si/3) - xi for xi in x]
                # lines_num_terms += [(num_terms, y)] # dlearn
                lines_num_terms += [(num_terms, z)] # elearn


# legend = ['kterm={0}, dim={1}, |S|={2}'.format(kterm, dim, si / 3) for kterm in range(mink, maxk, stepk) for dim in range(mind, maxd, stepd) for si in range(mins, maxs, steps)]
legend = ['kterm={0}, dim={1}, |S|={2}'.format(kterm, dim, int(ceil(si/3))) for kterm in range(mink, maxk, stepk) for dim in range(mind, maxd, stepd) for si in range(mins, maxs, steps)]


# plot_2D(x,y)
# plot_list_2D(lines_num_joins, legend=legend, xlabel='Number of joins', ylabel='|h| approx.', title="Growth of hypothesis' size", xscale='linear', yscale='log')  # dlearn
plot_list_2D(lines_num_joins, legend=legend, xlabel='Number of joins', ylabel="p(h'|f)", title='Error growth', xscale='linear', yscale='linear') # elearn

# plot_list_2D(lines_num_terms, legend=legend, xlabel='Number of terms', ylabel='|h| approx.', title="Growth of hypothesis' size", xscale='linear', yscale='log')  # dlearn
plot_list_2D(lines_num_terms, legend=legend, xlabel='Number of terms', ylabel="p(h'|f)", title='Error growth', xscale='linear', yscale='linear') # elearn