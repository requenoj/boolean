import unittest

from BoolLearnLib.Terms import *
from BoolLearnLib.Samples import *


##########
# Sample #
##########

class SamplesTestCase(unittest.TestCase):
    def setUp(self):
        self.target = Term('1**011*')
        self.hypot = Term('1**011*')

    def test_sample(self):
        self.sample1 = sample_from_DNF(8, self.target.vector)
        self.sample2 = sample_from_DNF(8, self.hypot.vector)
        self.assertLessEqual(self.sample1, self.sample2)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    unittest.main(testRunner=runner)
