import unittest

from BoolLearnLib.Terms import *


#########
# Terms #
#########

class TermsTestCase(unittest.TestCase):
    def setUp(self):
        self.a = Term('*1*011')
        self.b = Term('11**1*')

    def test_vector(self):
        # print '\nWe define a term %r with the following properties:' % (self.a.vector)
        # print '\nFor two terms %r and %r, we can compute the following:' % (self.a.vector, self.b.vector)
        print('\nWe define a term {0} with the following properties:'.format(str(self.a)))
        print('\nFor two terms {0} and {1}, we can compute the following:'.format(str(self.a), str(self.b)))

    def test_info(self):
        self.a.info()

    def test_intersection(self):
        r = intersection(self.a, self.b)
        print('Intersection: ' + str(r))
        self.assertEqual(r, Term('11*011'))

    def test_inclusion(self):
        r = inclusion(self.a, self.b)
        print('Inclusion: ' + str(r))
        self.assertFalse(r)

    def test_join(self):
        r = join(self.a, self.b)
        print('Join: ' + str(r))
        self.assertEqual(r, Term('*1**1*'))

    def test_join_cost(self):
        r = join_cost(self.a, self.b)
        print('join_cost: ' + str(r))
        self.assertEqual(r, 6)

    def test_hamming_distance(self):
        r = hamming(self.a, self.b)
        print('hamming distance: ' + str(r))
        self.assertEqual(r, 0)

    def test_hausdorf_distance(self):
        r = hausdorf_v(self.a, self.b)
        print('hausdorf distance: ' + str(r))
        self.assertEqual(r, 1)

    def test_symmetric_hausdorf_distance(self):
        r = hausdorf(self.a, self.b)
        print('symmetric hausdorf distance: ' + str(r))
        self.assertEqual(r, 2)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    unittest.main(testRunner=runner)
