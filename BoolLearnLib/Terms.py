# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:23:36 2017
last update on Wed 6 Jun 18

@author: elefther, nrequeno

Define Boolean Cubes (terms) and define properties and operations

"""

import numpy as np

import BoolLearnLib as BoolLog
from BoolLearnLib._py3k import range

# ==============================================================================
# Define the Cube
# ==============================================================================
def cube(n):
    """The alphabet. Returns the set of all points in B^n """
    assert type(n) == int and n >= 0
    s = '{0:0>' + str(n) + '}'
    return tuple([s.format(bin(i)[2:]) for i in range(2 ** n)])


# ==============================================================================
# Terms
# ==============================================================================

class Term:
    """A term is a sequence of certain length that contains symbols from {0,1,*}.
    The symbol * is used as a DON'T CARE symbol, that is, whenever a variable is of no interest and can take either value 0 or 1. """

    alphabet = {'0', '1', '*'}

    def __init__(self, vector):
        # type: (Term, str) -> None
        assert all(i in self.alphabet for i in vector)
        self.vector = vector

    # Printers
    def __repr__(self):
        # type: (Term) -> str
        return str(self.vector)

    def __str__(self):
        # type: (Term) -> str
        return self.vector

    # Equality functions
    def __eq__(self, other):
        # type: (Term, Term) -> bool
        return self.vector == other.vector

    def __ne__(self, other):
        # type: (Term, Term) -> bool
        return not self.__eq__(other)

    # Identity function (via hashing)
    def __hash__(self):
        # type: (Term) -> int
        return hash(self.vector)

    def dim(self):
        # type: (Term) -> int
        """Dimension of Boolean Cube"""
        return len(self.vector)

    def tau(self):
        # type: (Term) -> list
        """returns the type of the term, i.e., the set of DON'T CARE variables"""
        return [i for i in range(self.dim()) if self.vector[i] == '*']

    def order(self):
        # type: (Term) -> int
        """The number of DON'T CARE variables"""
        return self.vector.count('*')

    def size(self):
        # type: (Term) -> int
        """Cardinality of semantics"""
        return 2 ** self.order()

    def is_minterm(self):
        # type: (Term) -> bool
        """Returns True if the term is a minterm, False otherwise"""
        return not self.order()

    def semantics(self):
        return semantics(self.vector)

    def info(self):
        BoolLog.logger.info('Term {0}, of dimension {1}, is {2}a minterm.'.format(
            self.vector, self.dim(), str(np.where(self.is_minterm(), '', 'not '))))
        BoolLog.logger.info('It is of type {0} and has order {1}.'.format(self.tau(), self.order()))


# def semantics(vector):
#    stars = vector.count('*')
#    if stars == 0:
#        return [vector]
#    else:
#        return semantics(vector.replace('*','0',1)) + semantics(vector.replace('*','1',1))


def semantics(*terms):
    # type: (iter) -> set
    # terms can be a str, tuple, list, set or anything that allows iteration
    sem = set()
    for vector in terms:
        stars = vector.count('*')
        if stars == 0:
            sem.add(vector)
        else:
            sem |= semantics(vector.replace('*', '0', 1), vector.replace('*', '1', 1))
    return sem


def sem_size_appr(*terms):
    # type: (tuple) -> int
    return sum(2 ** term.count('*') for term in terms)
    # return sum([2 ** term.count('*') for term in terms])


def sem_size_exact(*terms):
    # type: (tuple) -> int
    return len(semantics(*terms))


# ==============================================================================
# Operations on Terms
# ==============================================================================

bit_cap = {('0', '0'): '0', ('0', '*'): '0',
           ('1', '1'): '1', ('1', '*'): '1',
           ('*', '0'): '0', ('*', '1'): '1', ('*', '*'): '*'}

bit_join = {('0', '0'): '0', ('0', '1'): '*', ('0', '*'): '*',
            ('1', '0'): '*', ('1', '1'): '1', ('1', '*'): '*',
            ('*', '0'): '*', ('*', '1'): '*', ('*', '*'): '*'}

bit_incl = {('0', '0'): True, ('0', '1'): False, ('0', '*'): True,
            ('1', '0'): False, ('1', '1'): True, ('1', '*'): True,
            ('*', '0'): False, ('*', '1'): False, ('*', '*'): True}

bit_hsdf = {('0', '0'): 0, ('0', '1'): 1, ('0', '*'): 0,
            ('1', '0'): 1, ('1', '1'): 0, ('1', '*'): 0,
            ('*', '0'): 1, ('*', '1'): 1, ('*', '*'): 0}

bit_hamm = {('0', '0'): 0, ('0', '1'): 1, ('0', '*'): 0,
            ('1', '0'): 1, ('1', '1'): 0, ('1', '*'): 0,
            ('*', '0'): 0, ('*', '1'): 0, ('*', '*'): 0}


def check(term1, term2):
    # type: (Term, Term) -> bool
    return term1.dim() == term2.dim()


def hamming(term1, term2):
    # type: (Term, Term) -> int
    """What is the Hamming distance of two terms"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum(i != j and not ('*' in (i, j)) for i, j in zip(term1.vector, term2.vector))


def hamming2(term1, term2):
    # type: (Term, Term) -> int
    """The Hamming distance of two terms computed bitwise from operation matrix"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum(bit_hamm.get((i, j)) for i, j in zip(term1.vector, term2.vector))
    # return sum([bit_hamm.get((i, j)) for i, j in zip(term1.vector, term2.vector)])


def hausdorf_v(term1, term2):
    # type: (Term, Term) -> int
    """The Hausdorf distance of two terms"""
    # check: the two vectors belong to the same domain
    assert check(term1, term2)
    return sum(bit_hsdf.get((i, j)) for i, j in zip(term1.vector, term2.vector))
    # return sum([bit_hsdf.get((i, j)) for i, j in zip(term1.vector, term2.vector)])


def hausdorf(term1, term2):
    # type: (Term, Term) -> int
    return max(hausdorf_v(term1, term2), hausdorf_v(term2, term1))


def intersection(term1, term2):
    # type: (Term, Term) -> Term
    """return a term that represents the intersection of two terms"""
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    r = [bit_cap.get((i, j), None) for i, j in zip(term1.vector, term2.vector)]
    if None in r:
        return None
    else:
        return Term(''.join(r))


# def union(term1,term2):
#    # check: the two terms belong to the same domain
#    assert term1.dim() == term2.dim()
#    pass

def inclusion(term1, term2):
    # type: (Term, Term) -> bool
    """return True (resp. False) when term1 is (resp. is not) included in term2"""
    assert check(term1, term2)
    return all(bit_incl[i] for i in zip(term1.vector, term2.vector))
    # return all([bit_incl[i] for i in zip(term1.vector, term2.vector)])


def join(term1, term2):
    # type: (Term, Term) -> Term
    """return a new term that represents the join of the two given terms"""
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    # r = [bit_join.get((i, j), None) for i, j in zip(term1.vector, term2.vector)]
    r = (bit_join.get((i, j), None) for i, j in zip(term1.vector, term2.vector))
    return Term(''.join(r))


def join_cost(term1, term2):
    # type: (Term, Term) -> int
    # check: the two terms belong to the same domain
    assert check(term1, term2)
    inters = intersection(term1, term2)
    if inters is None:
        return join(term1, term2).size() - term1.size() - term2.size()
    else:
        return join(term1, term2).size() - term1.size() - term2.size() + intersection(term1, term2).size()


def size(term1, term2):
    # type: (Term, Term) -> int
    """the size of the join defined as an external function"""
    assert check(term1, term2)
    return join(term1, term2).size()


##==============================================================================
## Define the graph of a Cube
##==============================================================================
#
# import networkx as nx
#
# def hypercube(n):
#    """Define a hypercube of dimension n"""
#    Q = nx.Graph()
#    Q.add_edge(0,1)
#
#    dim = 1
#    while dim < n :
#        # add one dimension to the hypercube
#        Q = nx.cartesian_product(Q,nx.Graph([(0,1)]))
#        dim += 1
#
#    # renaming the nodes
#    d = {a: ''.join([i for i in repr(a) if i not in "()', "]) for a in Q.nodes()}
#
#    return nx.relabel_nodes(Q,d)
#
#
##==============================================================================
## Graphical representation of two terms
##==============================================================================
#
# def make_graph(term1,term2):
#    """Make a graph for a term or union of terms"""
#    # check: the two terms belong to the same domain
#    assert check(term1,term2)
#
#    A = hypercube(term1.dim())
#
#    val_map = {}
#
#    # paint nodes of term blue
#    val_map.update({n:0.3 for n in term1.semantics()})
#
#    val_map.update({n:0.5 for n in term2.semantics()})
#
#    if not intersection(term1,term2) is None:
#        val_map.update({n:0.4 for n in intersection(term1,term2).semantics()})
#
#
#    node_colors = [val_map.get(node, 0.0) for node in A.nodes()]
#
#    nx.draw(A,node_size=100, cmap=plt.get_cmap('jet'), node_color=node_colors)
#

# ==============================================================================
# Vector inclusion in DNF (set of terms)
# ==============================================================================

def inclusion_dnf(vector, *dnf):
    """A Boolean function that return True when the vector is part of the DNF, False otherwise"""
    t1 = Term(vector)
    return any(inclusion(t1, Term(a)) for a in dnf)


def inclusion_dnf_old(vector, *dnf):
    """A Boolean function that return True when the vector is part of the DNF, False otherwise"""
    vector = Term(vector)
    for a in dnf:
        if inclusion(vector, Term(a)):
            return True
    return False
