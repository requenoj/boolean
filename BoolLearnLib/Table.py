# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:23:36 2017
last update on Wed 6 Jun 18

@author: elefther, nrequeno
"""

import time
import random

from sortedcontainers import SortedDict

import BoolLearnLib as BoolLog
from BoolLearnLib.Terms import Term, sem_size_appr, size, join, join_cost, hausdorf
from BoolLearnLib.Error import error

# ==============================================================================
# Boolean Function Inference
# ==============================================================================


class Table:
    """The data structure where we keep all information and do the calculations"""

    foo = {'dist': hausdorf, 'size': size, 'cost': join_cost, 'join': join}

    def __init__(self, sample, cost_function='cost'):
        # type: (Table, list, str) -> None
        """ Sample and cost functions are provided.
        The cost function can either be the 'join_cost' or the 'hamming distance'.
        A table calculates the cost for all pairs of data
        """

        assert cost_function in ['dist', 'cost'], " Cost function can either be the 'join_cost' or the hamming distance"

        self.terms = list(sample)

        self.cost_function = self.foo[cost_function]

        # a dictionary that will trace costs of all indices
        # self.cost = {}
        self.cost = SortedDict()
        self.update_cost()

        self.total_cost = 0
        self.timer = 0

        self.partitions = {i: {i} for i in sample}

    def reinitialization(self):
        # type: (Table) -> None
        self.__init__(self.initial_sample())

    def update_cost(self):
        # type: (Table) -> None
        for i, j in [(i, j) for i in self.terms for j in self.terms[self.terms.index(i) + 1:]]:
            self.cost[(i, j)] = self.cost_function(Term(i), Term(j))

    def current_sample(self):
        # type: (Table) -> iter
        return self.partitions.keys()

    def initial_sample(self):
        # type: (Table) -> list
        return [j for i in self.partitions.values() for j in i]

    # ==============================================================================
    # Learning functions
    # ==============================================================================
    # old name learn_kDNF
    def klearn(self, k, select='f', print_on=True, print_latex=False):
        # type: (Table, int, str, bool, bool) -> (list, list)
        """Returns a k-term DNF, i.e., a set of k terms"""

        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [sem_size_appr(*self.terms)]
        while len(self.partitions) > k:
            BoolLog.logger.info(len(self.partitions))
            if print_on:
                BoolLog.logger.info(self.__str__())
            if print_latex:
                BoolLog.logger.info(self.latex_output())
            self.step(select)
            X.append(S - len(self.partitions))
            Y.append(sem_size_appr(*self.terms))

        if print_on:
            BoolLog.logger.info(self.__str__())
        if print_latex:
            BoolLog.logger.info(self.latex_output())
        return X, Y

    # old name learn_e
    def elearn(self, testing_sample, e=0.1, select='f', print_on=True, print_latex=False):
        # type: (Table, list, float, str, bool, bool) -> (list, list, list)
        """Returns a k-term DNF, where k is unknown. The merging stops when
        the error (false negatives) become smaller than a given epsilon e (default value 0.1).
        The false negatives are computed based on some testing sample S1"""

        false_neg = error(self.terms, testing_sample)
        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [sem_size_appr(*self.terms)]
        Z = [false_neg]
        while false_neg > e and len(self.partitions) > 1:
            # print('partitions: %d, error: %.4f' % (len(self.partitions), false_neg))
            BoolLog.logger.info('partitions: {0}, error: {1:0.4f}'.format(len(self.partitions), false_neg))
            if print_on:
                BoolLog.logger.info(self.__str__())
            if print_latex:
                BoolLog.logger.info(self.latex_output())
            self.step(select)
            false_neg = error(self.terms, testing_sample)
            X.append(S - len(self.partitions))
            Y.append(sem_size_appr(*self.terms))
            Z.append(false_neg)

        BoolLog.logger.info('partitions: {0}, error: {1:0.4f}'.format(len(self.partitions), false_neg))
        if print_on:
            BoolLog.logger.info(self.__str__())
        if print_latex:
            BoolLog.logger.info(self.latex_output())
        return X, Y, Z

    def dlearn(self, threshold=2.0, select='f', print_on=True, print_latex=False):
        # type: (Table, float, str, bool, bool) -> (list, list)
        """Returns a k-term DNF, where k is unknown. The merging stops when
        the size of the hypothesis function makes a jump that is higher than a threshold. """

        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [sem_size_appr(*self.terms)]
        while len(self.partitions) > 1:
            BoolLog.logger.info('partitions: {0}, size: {0}'.format(len(self.partitions), Y[-1]))
            if print_on:
                BoolLog.logger.info(self.__str__())
            if print_latex:
                BoolLog.logger.info(self.latex_output())
            self.step(select)
            X.append(S - len(self.partitions))
            Y.append(sem_size_appr(*self.terms))

            BoolLog.logger.info(Y[-1] / float(Y[-2]))
            if Y[-1] / float(Y[-2]) > threshold:
                break

        BoolLog.logger.info('partitions: {0}, size: {0}'.format(len(self.partitions), Y[-1]))
        if print_on:
            BoolLog.logger.info(self.__str__())
        if print_latex:
            BoolLog.logger.info(self.latex_output())
        return X, Y

    def kdelearn(self, testing_sample, k, threshold=2.0, e=0.1, select='f', print_on=True, print_latex=False):
        # type: (Table, list, float, float, float, str, bool, bool) -> (list, list, list)
        """Returns a k-term DNF. The learning process stops when the first condition is met:
        - the merging reaches k-terms (stopping condition in klearn)
        - the size of the hypothesis function makes a jump that is higher than a threshold (stopping condition in dlearn)
        - the error (false negatives) become smaller than a given epsilon e (stopping condition in elearn)
        """

        false_neg = error(self.terms, testing_sample)
        S = len(self.initial_sample())
        X = [S - len(self.partitions)]
        Y = [sem_size_appr(*self.terms)]
        Z = [false_neg]
        while false_neg > e and len(self.partitions) > k:
            # print('partitions: %d, error: %.4f' % (len(self.partitions), false_neg))
            BoolLog.logger.info('partitions: {0}, size: {0}'.format(len(self.partitions), Y[-1]))
            if print_on:
                BoolLog.logger.info(self.__str__())
            if print_latex:
                BoolLog.logger.info(self.latex_output())
            self.step(select)
            false_neg = error(self.terms, testing_sample)
            X.append(S - len(self.partitions))
            Y.append(sem_size_appr(*self.terms))
            Z.append(false_neg)

            BoolLog.logger.info(Y[-1] / float(Y[-2]))
            if Y[-1] / float(Y[-2]) > threshold:
                break

        BoolLog.logger.info('partitions: {0}, error: {1:0.4f}'.format(len(self.partitions), false_neg))
        if print_on:
            BoolLog.logger.info(self.__str__())
        if print_latex:
            BoolLog.logger.info(self.latex_output())
        return X, Y, Z

    # ==============================================================================
    def step(self, select='f'):
        # type: (Table, str) -> (Term, Term)
        """finds a pair with minimal cost, and merge the terms"""

        t0_step = time.clock()
        if select in ['r', 'random']:
            ## select randomly among all the possible pairs
            tuple2merge = random.choice(self.mins())
        else:  # 'f' or 'first' or other then take first (which is faster)
            ## find the first pair of terms for which cost is minimum
            # tuple2merge = min(self.cost, key=self.cost.get)
            # Get the min element (specific operation of SortedDict)
            tuple2merge = self.cost.peekitem(0)[0]
            # tuple2merge = self.mins()[0]

        a = Term(tuple2merge[0])
        b = Term(tuple2merge[1])

        BoolLog.logger.info('Partitions = {0}. '.format(len(self.partitions)))
        BoolLog.logger.info('Merge {0} with {1}\n'.format(a.vector, b.vector))
        self.merge(a, b)
        BoolLog.logger.info('Gives {0}\n'.format(self.terms[-1]))

        self.timer += time.clock() - t0_step
        return a, b

    def merge(self, term1, term2):
        # type: (Table, Term, Term) -> _
        ### find join
        a = join(term1, term2)
        #        print a.vector
        self.total_cost += self.cost_function(term1, term2)

        ### update the sample
        # remove terms
        # self.terms.remove(term1.vector)
        # self.terms.remove(term2.vector)
        if term1.vector in self.terms:
            self.terms.remove(term1.vector)
        if term2.vector in self.terms:
            self.terms.remove(term2.vector)

        ### update the partition
        new_partition = self.partitions[term1.vector] | self.partitions[term2.vector]

        # current_sample = self.current_sample()
        # try:
        #     if term1.vector in current_sample:
        #         del self.partitions[term1.vector]
        #     if term2.vector in current_sample:
        #         del self.partitions[term2.vector]
        # except KeyError:
        #     print('term 1 ' + str(term1.vector))
        #     print('term 2 ' + str(term2.vector))
        #     print('current_sample ' + str(current_sample))
        #     print('in1 ' + str(term1.vector in current_sample))
        #     print('in2 ' + str(term2.vector in current_sample))
        #     raise KeyError

        current_sample = self.current_sample()
        if term1.vector in current_sample:
            del self.partitions[term1.vector]
        current_sample = self.current_sample()
        if term2.vector in current_sample:
            del self.partitions[term2.vector]

        # we need to put this after the deletion in case some partitions include some others
        self.partitions[a.vector] = new_partition

        ### update the cost table
        # delete old elements
        for key in [k for k in self.cost.keys() if term1.vector in k or term2.vector in k]:
            del self.cost[key]
        # add new ones
        for j in self.terms:
            b = Term(j)
            self.cost[(b.vector, a.vector)] = self.cost_function(a, b)

        # add join to the sample
        self.terms.append(a.vector)

    def mins(self):
        # type: (Table) -> list
        """Returns all the pairs with min cost"""

        # minval = min(self.cost.values()) # Standard dictionary
        minval = self.cost.peekitem(0)[1]  # SortedDict
        return [k for k, v in self.cost.items() if v == minval]

    def full_table(self):
        # type: (Table) -> dict
        """Creates a table (dictionary) that contains all information over joins, costs and sizes. """

        T = {'join': {}, 'size': {}}
        T['cost'] = self.cost
        for i, j in self.cost.keys():
            a = Term(i)
            b = Term(j)
            for key in ['join', 'size']:
                T[key][(a.vector, b.vector)] = self.foo[key](a, b)
        return T

    # ==============================================================================
    # Printers
    # ==============================================================================
    def latex_output(self):
        # type: (Table) -> str

        table = self.full_table()
        keys = table['cost'].keys()

        ## set length of columns
        columns_length = len(self.terms[0]) + 2
        table_rows = ['$$ \\begin{array}{| l |' + ' | '.join(['l' for i in self.terms]) + ' |} \n\\hline']

        table_rows.append(' ' * columns_length + ' & ' + ' & '.join(self.terms) + ' \\\\')
        table_rows.append('\\hline')

        for i in self.terms:
            row1 = []
            row2 = []
            for j in self.terms:
                if (i, j) in keys:
                    row1.append(str(table['join'][(i, j)]).center(columns_length))
                    row2.append(('(' + str(table['size'][(i, j)]) + ',' + str(table['cost'][(i, j)]) + ')').center(
                        columns_length))
                else:
                    row1.append('--'.center(columns_length))
                    row2.append(' '.center(columns_length))
            table_rows.append('  ' + i + ' &' + '&'.join(row1) + '\\\\')
            table_rows.append(' ' * columns_length + ' &' + '&'.join(row2) + '\\\\')
            table_rows.append('\\hline')

        table_rows.append('\\end{array} $$')
        s = ''
        for x in table_rows:
            s += x + '\n'
        return s

    def __str__(self):
        # type: (Table) -> str

        table = self.full_table()
        keys = table['cost'].keys()

        # set length of columns
        columns_length = len(self.terms[0]) + 2

        table_rows = [' ' * columns_length + ' | ' + ' | '.join(self.terms)]
        table_rows.append('-' * len(table_rows[0]) + '-')

        # fill in subsequent rows
        for i in self.terms:
            row1 = []
            row2 = []
            for j in self.terms:
                if (i, j) in keys:
                    row1.append(str(table['join'][(i, j)]).center(columns_length))
                    row2.append(('(' + str(table['size'][(i, j)]) + ',' + str(table['cost'][(i, j)]) + ')').center(
                        columns_length))
                else:
                    row1.append('--'.center(columns_length))
                    row2.append(' '.center(columns_length))
            table_rows.append('  ' + i + ' |' + '|'.join(row1))
            table_rows.append(' ' * columns_length + ' |' + '|'.join(row2))
            table_rows.append('-' * len(table_rows[0]) + '-')

        s = ''
        for x in table_rows:
            s += x + '\n'
        return s


# ==============================================================================
def project(rectangle, *sample):
    """project a sample on the rectangle"""
    indices = [i for i in range(len(rectangle)) if rectangle[i] == '*']
    return [''.join([s[i] for i in indices]) for s in sample]
